#include "lcd_OM6206_lib.h"
#include "font1.h"
#include "stm32l1xx_gpio.h"
#include "stm32l1xx_spi.h"

void LCDN_HwConfig(void)
{  //Hardware configuration (GPIO, SPI) for Nokia LCD
  GPIO_InitTypeDef  GPIO_InitStructure; 
  SPI_InitTypeDef   SPI_InitStructure;
  
  //GPIO config
  GPIO_InitStructure.GPIO_Pin =LCDN_CLK | LCDN_DIN | LCDN_DOU;    //SPI: SCK, MISO and MOSI lines
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_Init(LCDN_GPIO, &GPIO_InitStructure);

  GPIO_PinAFConfig(LCDN_GPIO, LCDN_CLK_SOURCE,GPIO_AF_SPI2);
  GPIO_PinAFConfig(LCDN_GPIO, LCDN_DIN_SOURCE,GPIO_AF_SPI2);
  GPIO_PinAFConfig(LCDN_GPIO, LCDN_DOU_SOURCE,GPIO_AF_SPI2);

  GPIO_InitStructure.GPIO_Pin = LCDN_SCE;                         //SPI: NSS line
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(LCDN_GPIO, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = LCDN_RES | LCDN_DC;               //RES, D/C lines
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(LCDN_GPIO, &GPIO_InitStructure);

  //SPI2 Config
  SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(LCDN_SPI, &SPI_InitStructure);
  //enable SPI2
  SPI_Cmd(LCDN_SPI, ENABLE);
} 


void LCDN_Init(void){
  //Display initialisaton
  GPIO_ResetBits(LCDN_GPIO, LCDN_RES);	//RES=0
  Delay(500);
  GPIO_SetBits(LCDN_GPIO, LCDN_RES);		//RES=1

  GPIO_ResetBits(LCDN_GPIO, LCDN_SCE);	//SCE=0 (ENABLE)
  GPIO_ResetBits(LCDN_GPIO, LCDN_DC);		//D/C=0 (ENABLE) - control commands will be transfered next

  LCDN_SPI_Transmit(0x21); //Extended instruction set (H)
  LCDN_SPI_Transmit(0xC0); //Vop setting
  LCDN_SPI_Transmit(0x06); //Temperature Coefficient
  LCDN_SPI_Transmit(0x13); //Set bias system
  LCDN_SPI_Transmit(0x0A); //HV-gen voltage multiplication
  LCDN_SPI_Transmit(0x20); //H=0 <- Basic Instruction set
  LCDN_SPI_Transmit(0x11); //VLCD Programming range
  LCDN_SPI_Transmit(0x0C); //display configuration
  LCDN_SPI_Transmit(0x40); //Set Y-Address of RAM (=0)
  LCDN_SPI_Transmit(0x80); //Set X-Address of RAM (=0)

  GPIO_SetBits(LCDN_GPIO, LCDN_SCE);		//D/C=1 - data will be transfered next
  GPIO_SetBits(LCDN_GPIO, LCDN_DC);		  //SCE=1 (DISABLE)  
}


void LCDN_Mode(unsigned char mode){
  //Display mode setting
  //negative (mode = 1) or normal (mode = 0) display mode

  GPIO_ResetBits(LCDN_GPIO, LCDN_SCE);	//SCE=0 (ENABLE)
  GPIO_ResetBits(LCDN_GPIO, LCDN_DC);		//D/C=0 (ENABLE) - control commands will be transfered next
  if (mode==1){
    LCDN_SPI_Transmit(0x0D);            //Negative mode
  } else {
    LCDN_SPI_Transmit(0x0C);            //Normal mode
  }
  GPIO_SetBits(LCDN_GPIO, LCDN_SCE);		//D/C=1 - data will be transfered next
  GPIO_SetBits(LCDN_GPIO, LCDN_DC);		  //SCE=1 (DISABLE) 
}


void LCDN_Clear(void){
 //Clear display
 unsigned char w,k;
 for (w=0;w<9;w++){
   LCDN_SetPos(0, w);
   for (k=0;k<96;k++){
     LCDN_SPI_Transmit(0x00);
   }
 }
}

void LCDN_WriteChar(unsigned char charCode, int mode){
  //Write one character
	//mode 1 = negative, 0 = normal
  unsigned int codeStart, codeEnd, i;
  int j;

  j = ((unsigned int)charCode)-LCDN_FONT_OFFSET;
  if (j>=0)
	  codeStart=((unsigned int)charCode)-LCDN_FONT_OFFSET
  else
	  codeStart = (unsigned int) charCode;
  codeStart=codeStart*LCDN_FONT_WIDTH;
  GPIO_SetBits(LCDN_GPIO, LCDN_DC);      //D/C=1 (ENABLE) - data will be transfered next
  GPIO_ResetBits(LCDN_GPIO, LCDN_SCE);	 //SCE=0 (ENABLE)
  codeEnd=codeStart+(unsigned int)LCDN_FONT_WIDTH;
  if (mode == 0)
  {
  for (i=codeStart; i<codeEnd; i++)
	  LCDN_SPI_Transmit(LCDN_Font[i]);
  }
  else
  {
  for (i=codeStart; i<codeEnd; i++)
	  LCDN_SPI_Transmit(~LCDN_Font[i]);
  }

  GPIO_SetBits(LCDN_GPIO, LCDN_DC);		  //SCE=1 (DISABLE)
}

void LCDN_WriteXY(const unsigned char *text, char x,char y, int mode){
  //Write text at XY position
	//mode 1 = negative, 0 = normal
  unsigned char textPixLength;

  textPixLength=x*LCDN_FONT_WIDTH;
  LCDN_SetPos(x,y);                     //Set position
  while(*text!=0) {
    LCDN_WriteChar(*text, mode);
    text++;
    textPixLength+=LCDN_FONT_WIDTH;
    if (textPixLength>=96){             //if next character is out of display, move to the next line
      LCDN_SetPos(x,++y);               //Set position
      textPixLength=x*LCDN_FONT_WIDTH;
    }
  }
} 


void LCDN_WriteXYSymbol(enum SYMBOL s, char x,char y, int mode){
  //Write symbol at XY position
	//mode 1 = negative, 0 = normal
  unsigned char textPixLength;
  textPixLength=x*LCDN_FONT_WIDTH;
  LCDN_SetPos(x,y);                     //Set position
  switch(s)
  {
  	  case STRZALKA_GORA:
  		  LCDN_WriteChar(STRZALKA_GORA, mode);
  		  break;
  	  case STRZALKA_DOL:
  		  LCDN_WriteChar(STRZALKA_DOL, mode);
  		  break;

  }
  }


void LCDN_SetPos(unsigned char x,unsigned char y){
  //Set XY position, counted in char size
  GPIO_ResetBits(LCDN_GPIO, LCDN_DC);	  //D/C=0 (ENABLE) - control commands will be transfered next
  GPIO_ResetBits(LCDN_GPIO, LCDN_SCE);	//SCE=0 (ENABLE)
  LCDN_SPI_Transmit(y | 0x40);          //rows counter reset
  x=x*LCDN_FONT_WIDTH;
  LCDN_SPI_Transmit(x | 0x80);          //columns counter reset
  GPIO_SetBits(LCDN_GPIO, LCDN_SCE);		//D/C=1 - data will be transfered next
  GPIO_SetBits(LCDN_GPIO, LCDN_DC);		  //SCE=1 (DISABLE)
}


void LCDN_SetPosG(unsigned char x,unsigned char y){
  //Set XY position, X counted in pixels
  GPIO_ResetBits(LCDN_GPIO, LCDN_DC);	  //D/C=0 (ENABLE) - control commands will be transfered next
  GPIO_ResetBits(LCDN_GPIO, LCDN_SCE);  //SCE=0 (ENABLE)
  LCDN_SPI_Transmit(y | 0x40);          //rows counter reset
  LCDN_SPI_Transmit(x | 0x80);          //columns counter reset
  GPIO_SetBits(LCDN_GPIO, LCDN_SCE);		//D/C=1 - data will be transfered next
  GPIO_SetBits(LCDN_GPIO, LCDN_DC);		  //SCE=1 (DISABLE)
}


void LCDN_WriteBMP(const unsigned char *buffer){	
  //Write (display) bitmap, bitmap must have 504 words
  unsigned int w,k;

  LCDN_SetPosG(0,0);               
  k=w=0;
  for(w=0;w<8;w++){
    LCDN_SetPosG(0,w);                  //set next row
    for(k=0;k<96;k++)
      LCDN_SPI_Transmit(*buffer++);	
  }
  GPIO_SetBits(LCDN_GPIO, LCDN_DC);	    //SCE=1 (DISABLE)
} 


void LCDN_SPI_Transmit(char cData) {  
 //transmit datum to display on SPI1
  while (SPI_I2S_GetFlagStatus(LCDN_SPI, SPI_I2S_FLAG_BSY) == SET);    //SCE changes state for every datum, this slows down transmission a bit, but reduces errors
  GPIO_ResetBits(LCDN_GPIO, LCDN_SCE);  //SCE=0 (ENABLE)
  SPI_I2S_SendData(LCDN_SPI, cData);
  while (SPI_I2S_GetFlagStatus(LCDN_SPI, SPI_I2S_FLAG_BSY) == SET);
  GPIO_SetBits(LCDN_GPIO, LCDN_SCE);		//SCE=1 (DISABLE)
}

