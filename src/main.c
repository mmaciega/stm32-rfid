/* Includes */
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "stm32l1xx.h"
#include "lcd_OM6206_lib.h"
#include "discover_board.h"

/* Private typedef */

/* Private define  */
#define RFID_SZTUK 3

/* Private macro */

/* Private variables */
GPIO_InitTypeDef  GPIO_InitStructure;
RCC_ClocksTypeDef RCC_Clocks;
NVIC_InitTypeDef NVIC_InitStructure;
unsigned char sek;
char karty[RFID_SZTUK][11] = { "NULL", "NULL", "NULL"};
unsigned short int czas_aktywnosci[RFID_SZTUK][3] = { {0,0,0}, {0,0,0}, {0,0,0} }; // [3] = [0] - godziny, [1] - minuty, [2] - sekundy
char snbuf[10];	// pomocniczy bufor na odczytany numer seryjny
static volatile uint32_t TimingDelay;
int odebrano_polecenie = 0;

/* Private function prototypes */
void Init_GPIOs (void);
void RCC_Configuration(void);
void RTC_Configuration(void);
void Config_USART(void);
void Interrupts_Configuration(void);
void WyswietlCzas(unsigned int* nrkarty, unsigned char* wiersz);			//wyswietlenie czasu aktynowsci dla danej karty
void WyswietlCzesc(int wart, unsigned int* nrkarty, unsigned char* wiersz);
void Delay(uint32_t nTime);
void TimingDelay_Decrement(void);

/* Private functions */
 void Config_Systick()
 {
   RCC_GetClocksFreq(&RCC_Clocks);
   SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);		//przerwanie co 1ms
 }

/*
--------------				MAIN 			------------------------
*/
int main(void)
{
  /* --------- konfiguracja mikroprocesora --------- */
  RCC_Configuration();			//konfiguracja RCC
  RTC_Configuration();			//konfiguracja RTC
  Init_GPIOs ();				//zainicjowanie GPIO
  Config_USART();				//konfiguracja USART
  Interrupts_Configuration();	//konfiguracaj przerwa� NVIC
  Config_Systick();				//konfiguracja przerwania Systick

  /* --------- konfiguracja wyswietlacza --------- */
  LCDN_HwConfig();				//obsluzenie GPIO i SPI wyswietlacza
  LCDN_Init();					//zainicjowanie wyswietlacza
  LCDN_Clear();					//wyczyszczenie wyswietlacza

  /* ------------- zmienne lokalne -------------- */
  unsigned char wiersz;
  unsigned int nrkarty,znal;


  /* ------------- glowna petla programu -------------- */
  while (1)
  {
	  	  /* ---------------------- wyswietlanie danych ------------------------------- */
	    LCDN_WriteXY((const unsigned char*) "  ID BRELOCZKOW ", 0x0, 0x0,1);	//wyswietlenie nag�owka
	    if (sek)																//gdy minie 1 sekunda aktualizuj czas aktywnych breloczkow
	    	AktualizujCzas();
		wiersz = 1;																//zaczynamy wypisywanie aktywnych ID od 1 wiersza (0 wiersz = naglowek)
		for(nrkarty=0;nrkarty<RFID_SZTUK; nrkarty++)
		{
			if(strcmp(&(karty[nrkarty][0]), "NULL")!=0)							//spradzamy czy pod indeksem "nrkarty" znajduje sie jakies zaaktywowane ID
			{
				LCDN_WriteXY((const unsigned char*) &(karty[nrkarty][0]), 0x0, wiersz, 0);			//wyswietla ID zaaktywowanego breloczka
				WyswietlCzas(&nrkarty, &wiersz);
			}
		}

		/* ------------------------ obsluga odebrania ID z modulu RFID	----------------- */
		if(odebrano_polecenie)
		{
			znal = -1;

			for(nrkarty=0;(nrkarty<RFID_SZTUK)&&(znal!=0); nrkarty++)				//sprawdzanie czy ID breloczka aktualnie znajdujace sie w buforze jest
				znal = strcmp(&(karty[nrkarty][0]), snbuf);							//jest juz zaaktywowane

			if(znal == 0)															//ID znajdujace sie w buforze jest zaaktywowane
			{
				strcpy(&(karty[nrkarty-1][0]), "NULL");								//usuniecie z pamieci ID breloczka (deaktywacja)
				czas_aktywnosci[nrkarty-1][0]=0;
				czas_aktywnosci[nrkarty-1][1]=0;
				czas_aktywnosci[nrkarty-1][2]=0;
			}
			else																	//ID znajdujace sie w buforze nie jest zaaktywowane
			{
				for(nrkarty=0;(nrkarty<RFID_SZTUK)&&(znal!=0); nrkarty++)			//szukamy wolnego miejsca w tablicy
					znal = strcmp(&(karty[nrkarty][0]), "NULL");
				strcpy(&(karty[nrkarty-1][0]), snbuf);								//kopiowanie ID znajduj�cego si� w buforze to wolnego miejsc w tablicy (aktywacja breloczka)
			}


			LCDN_Clear();															//czyszczenie wyswietlacza

		  	  /* ---------------------- wyswietlanie danych ------------------------------- */
		    LCDN_WriteXY((const unsigned char*) "  ID BRELOCZKOW ", 0x0, 0x0,1);
			wiersz = 1;																//zaczynamy wypisywanie aktywnych ID od 1 wiersza (0 wiersz = naglowek)
			for(nrkarty=0;nrkarty<RFID_SZTUK; nrkarty++)
			{
				if(strcmp(&(karty[nrkarty][0]), "NULL")!=0)							//spradzamy czy pod indeksem "nrkarty" znajduje sie jakies zaaktywowane ID
				{
					LCDN_WriteXY((const unsigned char*) &(karty[nrkarty][0]), 0x0, wiersz, 0);			//wyswietla ID zaaktywowanego breloczka
					WyswietlCzas(&nrkarty, &wiersz);
				}
			}
			Delay(700);																//op�nienie (700ms), s�u�y do zablokowania ponownego szybkiego przeslania ID breloczka do bufora
			odebrano_polecenie = 0;													//obsluzylismy ID z bufora, mozna zblizyc kolejka karte/breloczek
		}
  }

return 0;
}
/**
  * @brief  Configures the different system clocks.
  * @param  None
  * @retval None
  */
void RCC_Configuration(void)
{
  /* Enable the GPIOs Clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC| RCC_AHBPeriph_GPIOD| RCC_AHBPeriph_GPIOE| RCC_AHBPeriph_GPIOH, ENABLE);

  /* Enable comparator clock, SPI2, PWR */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP | RCC_APB1Periph_SPI2 | RCC_APB1Periph_PWR,ENABLE);

  /* Enable SYSCFG, USART1*/
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG | RCC_APB2Periph_USART1, ENABLE);
}

/**
  * @brief  Configures the real-time clock.
  * @param  None
  * @retval None
  */
void RTC_Configuration(void)
{
	 /* Allow access to the RTC */
	  PWR_RTCAccessCmd(ENABLE);

	  /* Reset Backup Domain */
	  RCC_RTCResetCmd(ENABLE);
	  RCC_RTCResetCmd(DISABLE);

	  /*! LSE Enable */
	  RCC_LSEConfig(RCC_LSE_ON);

	  /*! Wait till LSE is ready */
	  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
	  {}

	  /*! LCD Clock Source Selection */
	  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
}

/**
  * @brief  To initialize the I/O ports
  * @caller main
  * @param None
  * @retval None
  */

void  Init_GPIOs (void)
{
	/*	-----------------	USART1	-----------------------  */
    //Konfiguracja PA10 jako Rx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10,GPIO_AF_USART1) ;
}

void Config_USART()
{
	// Konfiguracja USART1
	USART_InitTypeDef USART_InitStructure;

	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx;
    USART_Init(USART1, &USART_InitStructure);

    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
    USART_Cmd(USART1,ENABLE);
}

void Interrupts_Configuration()
{
	#ifdef  VECT_TAB_RAM
		// Jezeli tablica wektorow w RAM, to ustaw jej adres na 0x20000000
		NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0);
	#else  // VECT_TAB_FLASH
		// W przeciwnym wypadku ustaw na 0x08000000
		NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);
	#endif

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief  Aktuzalicja czasu aktywnosci zaaktywowanych breloczkow/kart (co 1 sek)
  * @param  None
  * @retval None
  */
void AktualizujCzas()
{
	int nrkarty;
	for(nrkarty=0;nrkarty<RFID_SZTUK; nrkarty++)
	{
		if(strcmp(&(karty[nrkarty][0]), "NULL")!=0)		//spradzamy czy pod indeksem "nrkarty" znajduje sie jakies zaaktywowane ID
		{
			czas_aktywnosci[nrkarty][2]+=1;
			// zwi�kszenie liczby sekund aktywnosci breloczka pod indeksem "nrkarty"
			if(czas_aktywnosci[nrkarty][2]>=60)
			{
			// min�a minuta
				czas_aktywnosci[nrkarty][2]=0;
				// zerowanie sekund
				czas_aktywnosci[nrkarty][1]+=1;
				// zwi�kszenie minut o 1
			}
			if(czas_aktywnosci[nrkarty][1]>=60)
			// min�a godzina
			{
				czas_aktywnosci[nrkarty][1]=0;
				// zerowanie minut
				czas_aktywnosci[nrkarty][0]+=1;
				// zwi�kszenie godzin o 1
			}
		}
	}
	sek = 0;
}


/**
  * @brief  Wyswietlanie czasu aktywnosci zaaktywowanych breloczkow/kart
  * @param  nrkarty - nr. karty w tablicy dla kt�rej chcemy wyswietlic czas
  * 		wiersz - w ktorym wierszu ma zaczac wyswietlac
  * @retval None
  */
void WyswietlCzas(unsigned int* nrkarty, unsigned char* wiersz)
{
	LCDN_WriteXY((const unsigned char*) "Czas: ", 1, ++(*wiersz), 0);

	//wyswietlanie godzin
	WyswietlCzesc(0, nrkarty, wiersz);

	//wyswietlanie minut
	WyswietlCzesc(1, nrkarty, wiersz);

	//wyswietlanie sekund
	WyswietlCzesc(2, nrkarty, wiersz);

	//zwiekszenie aktualnego wiersza o 1
	++(*wiersz);
}

/**
  * @brief  Wyswietlanie cz�sci czasu aktywnosci zaaktywowanych breloczkow/kart
  * @param  wart - kt�r� czesc ma wypisac, 0 oznacza godziny, 1 - minuty, 2 - sekundy
  * 		nrkarty - nr. karty w tablicy dla kt�rej chcemy wyswietlic czas
  * 		wiersz - w ktorym wierszu ma zaczac wyswietlac
  * @retval None
  */
void WyswietlCzesc(int wart, unsigned int* nrkarty, unsigned char* wiersz)
{
	//wart, 0 oznacza godziny, 1 - minuty, 2 - sekundy
	char bufor[8];				//do zamiany int na char*

	if(czas_aktywnosci[*nrkarty][wart] == 0)
		LCDN_WriteXY((const unsigned char*) "00", 8+(wart*3), *wiersz, 0);
	else if(czas_aktywnosci[*nrkarty][wart] < 10)
	{
		LCDN_WriteXY((const unsigned char*) "0", 8+(wart*3), *wiersz, 0);
		snprintf(bufor, 8,"%d",czas_aktywnosci[*nrkarty][wart]);
		LCDN_WriteXY((const unsigned char*) bufor, 9+(wart*3), *wiersz, 0);
	}
	else
	{
		snprintf(bufor, 8,"%d",czas_aktywnosci[*nrkarty][wart]);
		LCDN_WriteXY((const unsigned char*) bufor, 8+(wart*3), *wiersz, 0);
	}

	if(wart != 2)
		LCDN_WriteXY((const unsigned char*) ":", 10+(wart*3), *wiersz, 0);
}

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in 10 ms.
  * @retval None
  */
void Delay(uint32_t nTime)
{
  TimingDelay = nTime;

  while(TimingDelay != 0);

}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{

  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
}

