#define LCDN_GPIO GPIOB      //LCD GPIO port
#define LCDN_SPI  SPI2       //LCD SPI port

#define LCDN_CLK GPIO_Pin_13  //SPI SCK  = LCD CLK line
#define LCDN_DIN GPIO_Pin_15  //SPI MOSI = LCD DIN line
#define LCDN_DOU GPIO_Pin_14  //SPI MISO  - unused, only MCU->LCD communication
#define LCDN_SCE GPIO_Pin_12  //SPI SS   = LCD SCE line
#define LCDN_RES GPIO_Pin_10  //           LCD RES line
#define LCDN_DC  GPIO_Pin_11  //           LCD D/C line
#define LCDN_CLK_SOURCE GPIO_PinSource13
#define LCDN_DIN_SOURCE GPIO_PinSource15
#define LCDN_DOU_SOURCE GPIO_PinSource14
#define LCDN_SCE_SOURCE GPIO_PinSource12
#define LCDN_RES_SOURCE GPIO_PinSource10
#define LCDN_DC_SOURCE  GPIO_PinSource_11
  
//symbole
enum SYMBOL {STRZALKA_GORA, STRZALKA_DOL};

void LCDN_HwConfig(void);
void LCDN_Init(void);
void LCDN_Mode(unsigned char mode);
void LCDN_Clear(void);
void LCDN_WriteChar(unsigned char charCode, int mode);
void LCDN_WriteXY(const unsigned char *text, char x,char y, int mode);
void LCDN_WriteXYSymbol(enum SYMBOL s, char x,char y, int mode);
void LCDN_SetPos(unsigned char x, unsigned char y);
void LCDN_SetPosG(unsigned char x, unsigned char y);
void LCDN_WriteBMP(const unsigned char *buffer);
void LCDN_SPI_Transmit(char cData);

