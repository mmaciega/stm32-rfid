# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c"
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\.coverage\\data//"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c"
# 80 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c"
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_wwdg.h" 1
# 32 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_wwdg.h"
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/stm32l1xx.h" 1
# 129 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/stm32l1xx.h"
typedef enum IRQn
{

  NonMaskableInt_IRQn = -14,
  MemoryManagement_IRQn = -12,
  BusFault_IRQn = -11,
  UsageFault_IRQn = -10,
  SVCall_IRQn = -5,
  DebugMonitor_IRQn = -4,
  PendSV_IRQn = -2,
  SysTick_IRQn = -1,


  WWDG_IRQn = 0,
  PVD_IRQn = 1,
  TAMPER_STAMP_IRQn = 2,
  RTC_WKUP_IRQn = 3,
  FLASH_IRQn = 4,
  RCC_IRQn = 5,
  EXTI0_IRQn = 6,
  EXTI1_IRQn = 7,
  EXTI2_IRQn = 8,
  EXTI3_IRQn = 9,
  EXTI4_IRQn = 10,
  DMA1_Channel1_IRQn = 11,
  DMA1_Channel2_IRQn = 12,
  DMA1_Channel3_IRQn = 13,
  DMA1_Channel4_IRQn = 14,
  DMA1_Channel5_IRQn = 15,
  DMA1_Channel6_IRQn = 16,
  DMA1_Channel7_IRQn = 17,
  ADC1_IRQn = 18,
  USB_HP_IRQn = 19,
  USB_LP_IRQn = 20,
  DAC_IRQn = 21,
  COMP_IRQn = 22,
  EXTI9_5_IRQn = 23,
  LCD_IRQn = 24,
  TIM9_IRQn = 25,
  TIM10_IRQn = 26,
  TIM11_IRQn = 27,
  TIM2_IRQn = 28,
  TIM3_IRQn = 29,
  TIM4_IRQn = 30,
  I2C1_EV_IRQn = 31,
  I2C1_ER_IRQn = 32,
  I2C2_EV_IRQn = 33,
  I2C2_ER_IRQn = 34,
  SPI1_IRQn = 35,
  SPI2_IRQn = 36,
  USART1_IRQn = 37,
  USART2_IRQn = 38,
  USART3_IRQn = 39,
  EXTI15_10_IRQn = 40,
  RTC_Alarm_IRQn = 41,
  USB_FS_WKUP_IRQn = 42,
  TIM6_IRQn = 43,
  TIM7_IRQn = 44
} IRQn_Type;





# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h" 1
# 90 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
# 1 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/include/stdint.h" 1 3 4


# 1 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 1 3 4
# 41 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
typedef signed char int8_t ;
typedef unsigned char uint8_t ;




typedef signed char int_least8_t;
typedef unsigned char uint_least8_t;




typedef signed short int16_t;
typedef unsigned short uint16_t;
# 67 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
typedef int16_t int_least16_t;
typedef uint16_t uint_least16_t;
# 79 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
typedef signed long int32_t;
typedef unsigned long uint32_t;
# 97 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
typedef int32_t int_least32_t;
typedef uint32_t uint_least32_t;
# 119 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
typedef signed long long int64_t;
typedef unsigned long long uint64_t;
# 129 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
typedef int64_t int_least64_t;
typedef uint64_t uint_least64_t;
# 159 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
  typedef signed int int_fast8_t;
  typedef unsigned int uint_fast8_t;




  typedef signed int int_fast16_t;
  typedef unsigned int uint_fast16_t;




  typedef signed int int_fast32_t;
  typedef unsigned int uint_fast32_t;
# 213 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
  typedef int_least64_t int_fast64_t;
  typedef uint_least64_t uint_fast64_t;







  typedef long long int intmax_t;
# 231 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
  typedef long long unsigned int uintmax_t;
# 243 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/../../../../arm-atollic-eabi/include/stdint.h" 3 4
typedef signed int intptr_t;
typedef unsigned int uintptr_t;
# 4 "c:\\program files (x86)\\atollic\\truestudio for stmicroelectronics stm32 lite 2.3.0\\armtools\\bin\\../lib/gcc/arm-atollic-eabi/4.5.1/include/stdint.h" 2 3 4
# 91 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h" 2
# 132 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
typedef struct
{
  volatile uint32_t ISER[8];
       uint32_t RESERVED0[24];
  volatile uint32_t ICER[8];
       uint32_t RSERVED1[24];
  volatile uint32_t ISPR[8];
       uint32_t RESERVED2[24];
  volatile uint32_t ICPR[8];
       uint32_t RESERVED3[24];
  volatile uint32_t IABR[8];
       uint32_t RESERVED4[56];
  volatile uint8_t IP[240];
       uint32_t RESERVED5[644];
  volatile uint32_t STIR;
} NVIC_Type;







typedef struct
{
  volatile const uint32_t CPUID;
  volatile uint32_t ICSR;
  volatile uint32_t VTOR;
  volatile uint32_t AIRCR;
  volatile uint32_t SCR;
  volatile uint32_t CCR;
  volatile uint8_t SHP[12];
  volatile uint32_t SHCSR;
  volatile uint32_t CFSR;
  volatile uint32_t HFSR;
  volatile uint32_t DFSR;
  volatile uint32_t MMFAR;
  volatile uint32_t BFAR;
  volatile uint32_t AFSR;
  volatile const uint32_t PFR[2];
  volatile const uint32_t DFR;
  volatile const uint32_t ADR;
  volatile const uint32_t MMFR[4];
  volatile const uint32_t ISAR[5];
} SCB_Type;
# 365 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
typedef struct
{
  volatile uint32_t CTRL;
  volatile uint32_t LOAD;
  volatile uint32_t VAL;
  volatile const uint32_t CALIB;
} SysTick_Type;
# 410 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
typedef struct
{
  volatile union
  {
    volatile uint8_t u8;
    volatile uint16_t u16;
    volatile uint32_t u32;
  } PORT [32];
       uint32_t RESERVED0[864];
  volatile uint32_t TER;
       uint32_t RESERVED1[15];
  volatile uint32_t TPR;
       uint32_t RESERVED2[15];
  volatile uint32_t TCR;
       uint32_t RESERVED3[29];
  volatile uint32_t IWR;
  volatile uint32_t IRR;
  volatile uint32_t IMCR;
       uint32_t RESERVED4[43];
  volatile uint32_t LAR;
  volatile uint32_t LSR;
       uint32_t RESERVED5[6];
  volatile const uint32_t PID4;
  volatile const uint32_t PID5;
  volatile const uint32_t PID6;
  volatile const uint32_t PID7;
  volatile const uint32_t PID0;
  volatile const uint32_t PID1;
  volatile const uint32_t PID2;
  volatile const uint32_t PID3;
  volatile const uint32_t CID0;
  volatile const uint32_t CID1;
  volatile const uint32_t CID2;
  volatile const uint32_t CID3;
} ITM_Type;
# 503 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
typedef struct
{
       uint32_t RESERVED0;
  volatile const uint32_t ICTR;



       uint32_t RESERVED1;

} InterruptType_Type;
# 535 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
typedef struct
{
  volatile const uint32_t TYPE;
  volatile uint32_t CTRL;
  volatile uint32_t RNR;
  volatile uint32_t RBAR;
  volatile uint32_t RASR;
  volatile uint32_t RBAR_A1;
  volatile uint32_t RASR_A1;
  volatile uint32_t RBAR_A2;
  volatile uint32_t RASR_A2;
  volatile uint32_t RBAR_A3;
  volatile uint32_t RASR_A3;
} MPU_Type;
# 620 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
typedef struct
{
  volatile uint32_t DHCSR;
  volatile uint32_t DCRSR;
  volatile uint32_t DCRDR;
  volatile uint32_t DEMCR;
} CoreDebug_Type;
# 1204 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void __enable_irq() { __asm volatile ("cpsie i"); }
static inline void __disable_irq() { __asm volatile ("cpsid i"); }

static inline void __enable_fault_irq() { __asm volatile ("cpsie f"); }
static inline void __disable_fault_irq() { __asm volatile ("cpsid f"); }

static inline void __NOP() { __asm volatile ("nop"); }
static inline void __WFI() { __asm volatile ("wfi"); }
static inline void __WFE() { __asm volatile ("wfe"); }
static inline void __SEV() { __asm volatile ("sev"); }
static inline void __ISB() { __asm volatile ("isb"); }
static inline void __DSB() { __asm volatile ("dsb"); }
static inline void __DMB() { __asm volatile ("dmb"); }
static inline void __CLREX() { __asm volatile ("clrex"); }
# 1227 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __get_PSP(void);
# 1237 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern void __set_PSP(uint32_t topOfProcStack);
# 1247 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __get_MSP(void);
# 1257 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern void __set_MSP(uint32_t topOfMainStack);
# 1266 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __get_BASEPRI(void);
# 1275 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern void __set_BASEPRI(uint32_t basePri);
# 1284 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __get_PRIMASK(void);
# 1293 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern void __set_PRIMASK(uint32_t priMask);
# 1302 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __get_FAULTMASK(void);
# 1311 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern void __set_FAULTMASK(uint32_t faultMask);
# 1320 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __get_CONTROL(void);
# 1329 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern void __set_CONTROL(uint32_t control);
# 1339 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __REV(uint32_t value);
# 1349 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __REV16(uint16_t value);
# 1359 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern int32_t __REVSH(int16_t value);
# 1369 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __RBIT(uint32_t value);
# 1379 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint8_t __LDREXB(uint8_t *addr);
# 1389 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint16_t __LDREXH(uint16_t *addr);
# 1399 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __LDREXW(uint32_t *addr);
# 1410 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __STREXB(uint8_t value, uint8_t *addr);
# 1421 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __STREXH(uint16_t value, uint16_t *addr);
# 1432 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern uint32_t __STREXW(uint32_t value, uint32_t *addr);
# 1468 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void NVIC_SetPriorityGrouping(uint32_t PriorityGroup)
{
  uint32_t reg_value;
  uint32_t PriorityGroupTmp = (PriorityGroup & 0x07);

  reg_value = ((SCB_Type *) ((0xE000E000) + 0x0D00))->AIRCR;
  reg_value &= ~((0xFFFFul << 16) | (7ul << 8));
  reg_value = (reg_value |
                (0x5FA << 16) |
                (PriorityGroupTmp << 8));
  ((SCB_Type *) ((0xE000E000) + 0x0D00))->AIRCR = reg_value;
}
# 1489 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline uint32_t NVIC_GetPriorityGrouping(void)
{
  return ((((SCB_Type *) ((0xE000E000) + 0x0D00))->AIRCR & (7ul << 8)) >> 8);
}
# 1502 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void NVIC_EnableIRQ(IRQn_Type IRQn)
{
  ((NVIC_Type *) ((0xE000E000) + 0x0100))->ISER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));
}
# 1515 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void NVIC_DisableIRQ(IRQn_Type IRQn)
{
  ((NVIC_Type *) ((0xE000E000) + 0x0100))->ICER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));
}
# 1529 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline uint32_t NVIC_GetPendingIRQ(IRQn_Type IRQn)
{
  return((uint32_t) ((((NVIC_Type *) ((0xE000E000) + 0x0100))->ISPR[(uint32_t)(IRQn) >> 5] & (1 << ((uint32_t)(IRQn) & 0x1F)))?1:0));
}
# 1542 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void NVIC_SetPendingIRQ(IRQn_Type IRQn)
{
  ((NVIC_Type *) ((0xE000E000) + 0x0100))->ISPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));
}
# 1555 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  ((NVIC_Type *) ((0xE000E000) + 0x0100))->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));
}
# 1569 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline uint32_t NVIC_GetActive(IRQn_Type IRQn)
{
  return((uint32_t)((((NVIC_Type *) ((0xE000E000) + 0x0100))->IABR[(uint32_t)(IRQn) >> 5] & (1 << ((uint32_t)(IRQn) & 0x1F)))?1:0));
}
# 1586 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void NVIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
  if(IRQn < 0) {
    ((SCB_Type *) ((0xE000E000) + 0x0D00))->SHP[((uint32_t)(IRQn) & 0xF)-4] = ((priority << (8 - 4)) & 0xff); }
  else {
    ((NVIC_Type *) ((0xE000E000) + 0x0100))->IP[(uint32_t)(IRQn)] = ((priority << (8 - 4)) & 0xff); }
}
# 1609 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline uint32_t NVIC_GetPriority(IRQn_Type IRQn)
{

  if(IRQn < 0) {
    return((uint32_t)(((SCB_Type *) ((0xE000E000) + 0x0D00))->SHP[((uint32_t)(IRQn) & 0xF)-4] >> (8 - 4))); }
  else {
    return((uint32_t)(((NVIC_Type *) ((0xE000E000) + 0x0100))->IP[(uint32_t)(IRQn)] >> (8 - 4))); }
}
# 1634 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline uint32_t NVIC_EncodePriority (uint32_t PriorityGroup, uint32_t PreemptPriority, uint32_t SubPriority)
{
  uint32_t PriorityGroupTmp = (PriorityGroup & 0x07);
  uint32_t PreemptPriorityBits;
  uint32_t SubPriorityBits;

  PreemptPriorityBits = ((7 - PriorityGroupTmp) > 4) ? 4 : 7 - PriorityGroupTmp;
  SubPriorityBits = ((PriorityGroupTmp + 4) < 7) ? 0 : PriorityGroupTmp - 7 + 4;

  return (
           ((PreemptPriority & ((1 << (PreemptPriorityBits)) - 1)) << SubPriorityBits) |
           ((SubPriority & ((1 << (SubPriorityBits )) - 1)))
         );
}
# 1665 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void NVIC_DecodePriority (uint32_t Priority, uint32_t PriorityGroup, uint32_t* pPreemptPriority, uint32_t* pSubPriority)
{
  uint32_t PriorityGroupTmp = (PriorityGroup & 0x07);
  uint32_t PreemptPriorityBits;
  uint32_t SubPriorityBits;

  PreemptPriorityBits = ((7 - PriorityGroupTmp) > 4) ? 4 : 7 - PriorityGroupTmp;
  SubPriorityBits = ((PriorityGroupTmp + 4) < 7) ? 0 : PriorityGroupTmp - 7 + 4;

  *pPreemptPriority = (Priority >> SubPriorityBits) & ((1 << (PreemptPriorityBits)) - 1);
  *pSubPriority = (Priority ) & ((1 << (SubPriorityBits )) - 1);
}
# 1694 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline uint32_t SysTick_Config(uint32_t ticks)
{
  if (ticks > (0xFFFFFFul << 0)) return (1);

  ((SysTick_Type *) ((0xE000E000) + 0x0010))->LOAD = (ticks & (0xFFFFFFul << 0)) - 1;
  NVIC_SetPriority (SysTick_IRQn, (1<<4) - 1);
  ((SysTick_Type *) ((0xE000E000) + 0x0010))->VAL = 0;
  ((SysTick_Type *) ((0xE000E000) + 0x0010))->CTRL = (1ul << 2) |
                   (1ul << 1) |
                   (1ul << 0);
  return (0);
}
# 1719 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline void NVIC_SystemReset(void)
{
  ((SCB_Type *) ((0xE000E000) + 0x0D00))->AIRCR = ((0x5FA << 16) |
                 (((SCB_Type *) ((0xE000E000) + 0x0D00))->AIRCR & (7ul << 8)) |
                 (1ul << 2));
  __DSB();
  while(1);
}
# 1742 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
extern volatile int ITM_RxBuffer;
# 1756 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline uint32_t ITM_SendChar (uint32_t ch)
{
  if ((((CoreDebug_Type *) (0xE000EDF0))->DEMCR & (1ul << 24)) &&
      (((ITM_Type *) (0xE0000000))->TCR & (1ul << 0)) &&
      (((ITM_Type *) (0xE0000000))->TER & (1ul << 0) ) )
  {
    while (((ITM_Type *) (0xE0000000))->PORT[0].u32 == 0);
    ((ITM_Type *) (0xE0000000))->PORT[0].u8 = (uint8_t) ch;
  }
  return (ch);
}
# 1778 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline int ITM_ReceiveChar (void) {
  int ch = -1;

  if (ITM_RxBuffer != 0x5AA55AA5) {
    ch = ITM_RxBuffer;
    ITM_RxBuffer = 0x5AA55AA5;
  }

  return (ch);
}
# 1798 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\CoreSupport/core_cm3.h"
static inline int ITM_CheckChar (void) {

  if (ITM_RxBuffer == 0x5AA55AA5) {
    return (0);
  } else {
    return (1);
  }
}
# 194 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/stm32l1xx.h" 2
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/system_stm32l1xx.h" 1
# 53 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/system_stm32l1xx.h"
extern uint32_t SystemCoreClock;
# 79 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/system_stm32l1xx.h"
extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);
# 195 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/stm32l1xx.h" 2






typedef enum {RESET = 0, SET = !RESET} FlagStatus, ITStatus;

typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;


typedef enum {ERROR = 0, SUCCESS = !ERROR} ErrorStatus;
# 260 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/stm32l1xx.h"
typedef struct
{
  volatile uint32_t SR;
  volatile uint32_t CR1;
  volatile uint32_t CR2;
  volatile uint32_t SMPR1;
  volatile uint32_t SMPR2;
  volatile uint32_t SMPR3;
  volatile uint32_t JOFR1;
  volatile uint32_t JOFR2;
  volatile uint32_t JOFR3;
  volatile uint32_t JOFR4;
  volatile uint32_t HTR;
  volatile uint32_t LTR;
  volatile uint32_t SQR1;
  volatile uint32_t SQR2;
  volatile uint32_t SQR3;
  volatile uint32_t SQR4;
  volatile uint32_t SQR5;
  volatile uint32_t JSQR;
  volatile uint32_t JDR1;
  volatile uint32_t JDR2;
  volatile uint32_t JDR3;
  volatile uint32_t JDR4;
  volatile uint32_t DR;
} ADC_TypeDef;

typedef struct
{
  volatile uint32_t CSR;
  volatile uint32_t CCR;
} ADC_Common_TypeDef;






typedef struct
{
  volatile uint32_t CSR;
} COMP_TypeDef;





typedef struct
{
  volatile uint32_t DR;
  volatile uint8_t IDR;
  uint8_t RESERVED0;
  uint16_t RESERVED1;
  volatile uint32_t CR;
} CRC_TypeDef;





typedef struct
{
  volatile uint32_t CR;
  volatile uint32_t SWTRIGR;
  volatile uint32_t DHR12R1;
  volatile uint32_t DHR12L1;
  volatile uint32_t DHR8R1;
  volatile uint32_t DHR12R2;
  volatile uint32_t DHR12L2;
  volatile uint32_t DHR8R2;
  volatile uint32_t DHR12RD;
  volatile uint32_t DHR12LD;
  volatile uint32_t DHR8RD;
  volatile uint32_t DOR1;
  volatile uint32_t DOR2;
  volatile uint32_t SR;
} DAC_TypeDef;





typedef struct
{
  volatile uint32_t IDCODE;
  volatile uint32_t CR;
  volatile uint32_t APB1FZ;
  volatile uint32_t APB2FZ;
}DBGMCU_TypeDef;





typedef struct
{
  volatile uint32_t CCR;
  volatile uint32_t CNDTR;
  volatile uint32_t CPAR;
  volatile uint32_t CMAR;
} DMA_Channel_TypeDef;

typedef struct
{
  volatile uint32_t ISR;
  volatile uint32_t IFCR;
} DMA_TypeDef;





typedef struct
{
  volatile uint32_t IMR;
  volatile uint32_t EMR;
  volatile uint32_t RTSR;
  volatile uint32_t FTSR;
  volatile uint32_t SWIER;
  volatile uint32_t PR;
} EXTI_TypeDef;





typedef struct
{
  volatile uint32_t ACR;
  volatile uint32_t PECR;
  volatile uint32_t PDKEYR;
  volatile uint32_t PEKEYR;
  volatile uint32_t PRGKEYR;
  volatile uint32_t OPTKEYR;
  volatile uint32_t SR;
  volatile uint32_t OBR;
  volatile uint32_t WRPR;
} FLASH_TypeDef;





typedef struct
{
  volatile uint32_t RDP;
  volatile uint32_t USER;
  volatile uint32_t WRP01;
  volatile uint32_t WRP23;
} OB_TypeDef;





typedef struct
{
  volatile uint32_t MODER;
  volatile uint16_t OTYPER;
  uint16_t RESERVED0;
  volatile uint32_t OSPEEDR;
  volatile uint32_t PUPDR;
  volatile uint16_t IDR;
  uint16_t RESERVED1;
  volatile uint16_t ODR;
  uint16_t RESERVED2;
  volatile uint16_t BSRRL;
  volatile uint16_t BSRRH;
  volatile uint32_t LCKR;
  volatile uint32_t AFR[2];
} GPIO_TypeDef;





typedef struct
{
  volatile uint32_t MEMRMP;
  volatile uint32_t PMC;
  volatile uint32_t EXTICR[4];
} SYSCFG_TypeDef;





typedef struct
{
  volatile uint16_t CR1;
  uint16_t RESERVED0;
  volatile uint16_t CR2;
  uint16_t RESERVED1;
  volatile uint16_t OAR1;
  uint16_t RESERVED2;
  volatile uint16_t OAR2;
  uint16_t RESERVED3;
  volatile uint16_t DR;
  uint16_t RESERVED4;
  volatile uint16_t SR1;
  uint16_t RESERVED5;
  volatile uint16_t SR2;
  uint16_t RESERVED6;
  volatile uint16_t CCR;
  uint16_t RESERVED7;
  volatile uint16_t TRISE;
  uint16_t RESERVED8;
} I2C_TypeDef;





typedef struct
{
  volatile uint32_t KR;
  volatile uint32_t PR;
  volatile uint32_t RLR;
  volatile uint32_t SR;
} IWDG_TypeDef;






typedef struct
{
  volatile uint32_t CR;
  volatile uint32_t FCR;
  volatile uint32_t SR;
  volatile uint32_t CLR;
  uint32_t RESERVED;
  volatile uint32_t RAM[16];
} LCD_TypeDef;





typedef struct
{
  volatile uint32_t CR;
  volatile uint32_t CSR;
} PWR_TypeDef;





typedef struct
{
  volatile uint32_t CR;
  volatile uint32_t ICSCR;
  volatile uint32_t CFGR;
  volatile uint32_t CIR;
  volatile uint32_t AHBRSTR;
  volatile uint32_t APB2RSTR;
  volatile uint32_t APB1RSTR;
  volatile uint32_t AHBENR;
  volatile uint32_t APB2ENR;
  volatile uint32_t APB1ENR;
  volatile uint32_t AHBLPENR;
  volatile uint32_t APB2LPENR;
  volatile uint32_t APB1LPENR;
  volatile uint32_t CSR;
} RCC_TypeDef;





typedef struct
{
  volatile uint32_t ICR;
  volatile uint32_t ASCR1;
  volatile uint32_t ASCR2;
  volatile uint32_t HYSCR1;
  volatile uint32_t HYSCR2;
  volatile uint32_t HYSCR3;
} RI_TypeDef;





typedef struct
{
  volatile uint32_t TR;
  volatile uint32_t DR;
  volatile uint32_t CR;
  volatile uint32_t ISR;
  volatile uint32_t PRER;
  volatile uint32_t WUTR;
  volatile uint32_t CALIBR;
  volatile uint32_t ALRMAR;
  volatile uint32_t ALRMBR;
  volatile uint32_t WPR;
  uint32_t RESERVED1;
  uint32_t RESERVED2;
  volatile uint32_t TSTR;
  volatile uint32_t TSDR;
  uint32_t RESERVED3;
  uint32_t RESERVED4;
  volatile uint32_t TAFCR;
  uint32_t RESERVED5;
  uint32_t RESERVED6;
  uint32_t RESERVED7;
  volatile uint32_t BKP0R;
  volatile uint32_t BKP1R;
  volatile uint32_t BKP2R;
  volatile uint32_t BKP3R;
  volatile uint32_t BKP4R;
  volatile uint32_t BKP5R;
  volatile uint32_t BKP6R;
  volatile uint32_t BKP7R;
  volatile uint32_t BKP8R;
  volatile uint32_t BKP9R;
  volatile uint32_t BKP10R;
  volatile uint32_t BKP11R;
  volatile uint32_t BKP12R;
  volatile uint32_t BKP13R;
  volatile uint32_t BKP14R;
  volatile uint32_t BKP15R;
  volatile uint32_t BKP16R;
  volatile uint32_t BKP17R;
  volatile uint32_t BKP18R;
  volatile uint32_t BKP19R;
} RTC_TypeDef;





typedef struct
{
  volatile uint16_t CR1;
  uint16_t RESERVED0;
  volatile uint16_t CR2;
  uint16_t RESERVED1;
  volatile uint16_t SR;
  uint16_t RESERVED2;
  volatile uint16_t DR;
  uint16_t RESERVED3;
  volatile uint16_t CRCPR;
  uint16_t RESERVED4;
  volatile uint16_t RXCRCR;
  uint16_t RESERVED5;
  volatile uint16_t TXCRCR;
  uint16_t RESERVED6;
} SPI_TypeDef;





typedef struct
{
  volatile uint16_t CR1;
  uint16_t RESERVED0;
  volatile uint16_t CR2;
  uint16_t RESERVED1;
  volatile uint16_t SMCR;
  uint16_t RESERVED2;
  volatile uint16_t DIER;
  uint16_t RESERVED3;
  volatile uint16_t SR;
  uint16_t RESERVED4;
  volatile uint16_t EGR;
  uint16_t RESERVED5;
  volatile uint16_t CCMR1;
  uint16_t RESERVED6;
  volatile uint16_t CCMR2;
  uint16_t RESERVED7;
  volatile uint16_t CCER;
  uint16_t RESERVED8;
  volatile uint16_t CNT;
  uint16_t RESERVED9;
  volatile uint16_t PSC;
  uint16_t RESERVED10;
  volatile uint16_t ARR;
  uint16_t RESERVED11;
  uint32_t RESERVED12;
  volatile uint16_t CCR1;
  uint16_t RESERVED13;
  volatile uint16_t CCR2;
  uint16_t RESERVED14;
  volatile uint16_t CCR3;
  uint16_t RESERVED15;
  volatile uint16_t CCR4;
  uint16_t RESERVED16;
  uint32_t RESERVED17;
  volatile uint16_t DCR;
  uint16_t RESERVED18;
  volatile uint16_t DMAR;
  uint16_t RESERVED19;
  volatile uint16_t OR;
  uint16_t RESERVED20;
} TIM_TypeDef;





typedef struct
{
  volatile uint16_t SR;
  uint16_t RESERVED0;
  volatile uint16_t DR;
  uint16_t RESERVED1;
  volatile uint16_t BRR;
  uint16_t RESERVED2;
  volatile uint16_t CR1;
  uint16_t RESERVED3;
  volatile uint16_t CR2;
  uint16_t RESERVED4;
  volatile uint16_t CR3;
  uint16_t RESERVED5;
  volatile uint16_t GTPR;
  uint16_t RESERVED6;
} USART_TypeDef;





typedef struct
{
  volatile uint32_t CR;
  volatile uint32_t CFR;
  volatile uint32_t SR;
} WWDG_TypeDef;
# 5099 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/stm32l1xx.h"
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 1
# 30 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h"
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_comp.h" 1
# 32 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_comp.h"
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/stm32l1xx.h" 1
# 33 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_comp.h" 2
# 48 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_comp.h"
typedef struct
{
  uint32_t COMP_Speed;

  uint32_t COMP_InvertingInput;

  uint32_t COMP_OutputSelect;


}COMP_InitTypeDef;
# 153 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_comp.h"
void COMP_DeInit(void);


void COMP_Init(COMP_InitTypeDef* COMP_InitStruct);
void COMP_Cmd(FunctionalState NewState);
uint8_t COMP_GetOutputLevel(uint32_t COMP_Selection);


void COMP_WindowCmd(FunctionalState NewState);


void COMP_VrefintOutputCmd(FunctionalState NewState);
# 31 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2


# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_dma.h" 1
# 48 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_dma.h"
typedef struct
{
  uint32_t DMA_PeripheralBaseAddr;

  uint32_t DMA_MemoryBaseAddr;

  uint32_t DMA_DIR;


  uint32_t DMA_BufferSize;



  uint32_t DMA_PeripheralInc;


  uint32_t DMA_MemoryInc;


  uint32_t DMA_PeripheralDataSize;


  uint32_t DMA_MemoryDataSize;


  uint32_t DMA_Mode;




  uint32_t DMA_Priority;


  uint32_t DMA_M2M;

}DMA_InitTypeDef;
# 331 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_dma.h"
void DMA_DeInit(DMA_Channel_TypeDef* DMAy_Channelx);


void DMA_Init(DMA_Channel_TypeDef* DMAy_Channelx, DMA_InitTypeDef* DMA_InitStruct);
void DMA_StructInit(DMA_InitTypeDef* DMA_InitStruct);
void DMA_Cmd(DMA_Channel_TypeDef* DMAy_Channelx, FunctionalState NewState);


void DMA_SetCurrDataCounter(DMA_Channel_TypeDef* DMAy_Channelx, uint16_t DataNumber);
uint16_t DMA_GetCurrDataCounter(DMA_Channel_TypeDef* DMAy_Channelx);


void DMA_ITConfig(DMA_Channel_TypeDef* DMAy_Channelx, uint32_t DMA_IT, FunctionalState NewState);
FlagStatus DMA_GetFlagStatus(uint32_t DMA_FLAG);
void DMA_ClearFlag(uint32_t DMA_FLAG);
ITStatus DMA_GetITStatus(uint32_t DMA_IT);
void DMA_ClearITPendingBit(uint32_t DMA_IT);
# 34 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_exti.h" 1
# 48 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_exti.h"
typedef enum
{
  EXTI_Mode_Interrupt = 0x00,
  EXTI_Mode_Event = 0x04
}EXTIMode_TypeDef;







typedef enum
{
  EXTI_Trigger_Rising = 0x08,
  EXTI_Trigger_Falling = 0x0C,
  EXTI_Trigger_Rising_Falling = 0x10
}EXTITrigger_TypeDef;
# 74 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_exti.h"
typedef struct
{
  uint32_t EXTI_Line;


  EXTIMode_TypeDef EXTI_Mode;


  EXTITrigger_TypeDef EXTI_Trigger;


  FunctionalState EXTI_LineCmd;

}EXTI_InitTypeDef;
# 163 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_exti.h"
void EXTI_DeInit(void);


void EXTI_Init(EXTI_InitTypeDef* EXTI_InitStruct);
void EXTI_StructInit(EXTI_InitTypeDef* EXTI_InitStruct);
void EXTI_GenerateSWInterrupt(uint32_t EXTI_Line);


FlagStatus EXTI_GetFlagStatus(uint32_t EXTI_Line);
void EXTI_ClearFlag(uint32_t EXTI_Line);
ITStatus EXTI_GetITStatus(uint32_t EXTI_Line);
void EXTI_ClearITPendingBit(uint32_t EXTI_Line);
# 35 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_flash.h" 1
# 47 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_flash.h"
typedef enum
{
  FLASH_BUSY = 1,
  FLASH_ERROR_WRP,
  FLASH_ERROR_PROGRAM,
  FLASH_COMPLETE,
  FLASH_TIMEOUT
}FLASH_Status;
# 287 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_flash.h"
void FLASH_SetLatency(uint32_t FLASH_Latency);
void FLASH_PrefetchBufferCmd(FunctionalState NewState);
void FLASH_ReadAccess64Cmd(FunctionalState NewState);
void FLASH_SLEEPPowerDownCmd(FunctionalState NewState);


void FLASH_Unlock(void);
void FLASH_Lock(void);
FLASH_Status FLASH_ErasePage(uint32_t Page_Address);
FLASH_Status FLASH_FastProgramWord(uint32_t Address, uint32_t Data);


void DATA_EEPROM_Unlock(void);
void DATA_EEPROM_Lock(void);
void DATA_EEPROM_FixedTimeProgramCmd(FunctionalState NewState);
FLASH_Status DATA_EEPROM_EraseWord(uint32_t Address);
FLASH_Status DATA_EEPROM_FastProgramByte(uint32_t Address, uint8_t Data);
FLASH_Status DATA_EEPROM_FastProgramHalfWord(uint32_t Address, uint16_t Data);
FLASH_Status DATA_EEPROM_FastProgramWord(uint32_t Address, uint32_t Data);
FLASH_Status DATA_EEPROM_ProgramByte(uint32_t Address, uint8_t Data);
FLASH_Status DATA_EEPROM_ProgramHalfWord(uint32_t Address, uint16_t Data);
FLASH_Status DATA_EEPROM_ProgramWord(uint32_t Address, uint32_t Data);


void FLASH_OB_Unlock(void);
void FLASH_OB_Lock(void);
void FLASH_OB_Launch(void);
FLASH_Status FLASH_OB_WRPConfig(uint32_t OB_WRP, FunctionalState NewState);
FLASH_Status FLASH_OB_RDPConfig(uint8_t OB_RDP);
FLASH_Status FLASH_OB_UserConfig(uint8_t OB_IWDG, uint8_t OB_STOP, uint8_t OB_STDBY);
FLASH_Status FLASH_OB_BORConfig(uint8_t OB_BOR);
uint8_t FLASH_OB_GetUser(void);
uint32_t FLASH_OB_GetWRP(void);
FlagStatus FLASH_OB_GetRDP(void);
uint8_t FLASH_OB_GetBOR(void);


void FLASH_ITConfig(uint32_t FLASH_IT, FunctionalState NewState);
FlagStatus FLASH_GetFlagStatus(uint32_t FLASH_FLAG);
void FLASH_ClearFlag(uint32_t FLASH_FLAG);
FLASH_Status FLASH_GetStatus(void);
FLASH_Status FLASH_WaitForLastOperation(uint32_t Timeout);






FLASH_Status FLASH_RUNPowerDownCmd(FunctionalState NewState);
FLASH_Status FLASH_ProgramHalfPage(uint32_t Address, uint32_t* pBuffer);
FLASH_Status DATA_EEPROM_EraseDoubleWord(uint32_t Address);
FLASH_Status DATA_EEPROM_ProgramDoubleWord(uint32_t Address, uint64_t Data);
# 36 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_gpio.h" 1
# 54 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_gpio.h"
typedef enum
{
  GPIO_Mode_IN = 0x00,
  GPIO_Mode_OUT = 0x01,
  GPIO_Mode_AF = 0x02,
  GPIO_Mode_AN = 0x03
}GPIOMode_TypeDef;
# 70 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_gpio.h"
typedef enum
{ GPIO_OType_PP = 0x00,
  GPIO_OType_OD = 0x01
}GPIOOType_TypeDef;
# 83 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_gpio.h"
typedef enum
{
  GPIO_Speed_400KHz = 0x00,
  GPIO_Speed_2MHz = 0x01,
  GPIO_Speed_10MHz = 0x02,
  GPIO_Speed_40MHz = 0x03
}GPIOSpeed_TypeDef;
# 99 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_gpio.h"
typedef enum
{ GPIO_PuPd_NOPULL = 0x00,
  GPIO_PuPd_UP = 0x01,
  GPIO_PuPd_DOWN = 0x02
}GPIOPuPd_TypeDef;
# 113 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_gpio.h"
typedef enum
{ Bit_RESET = 0,
  Bit_SET
}BitAction;
# 126 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_gpio.h"
typedef struct
{
  uint32_t GPIO_Pin;


  GPIOMode_TypeDef GPIO_Mode;


  GPIOSpeed_TypeDef GPIO_Speed;


  GPIOOType_TypeDef GPIO_OType;


  GPIOPuPd_TypeDef GPIO_PuPd;

}GPIO_InitTypeDef;
# 330 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_gpio.h"
void GPIO_DeInit(GPIO_TypeDef* GPIOx);


void GPIO_Init(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_InitStruct);
void GPIO_StructInit(GPIO_InitTypeDef* GPIO_InitStruct);
void GPIO_PinLockConfig(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);


uint8_t GPIO_ReadInputDataBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
uint16_t GPIO_ReadInputData(GPIO_TypeDef* GPIOx);
uint8_t GPIO_ReadOutputDataBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
uint16_t GPIO_ReadOutputData(GPIO_TypeDef* GPIOx);
void GPIO_SetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void GPIO_ResetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void GPIO_WriteBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, BitAction BitVal);
void GPIO_Write(GPIO_TypeDef* GPIOx, uint16_t PortVal);


void GPIO_PinAFConfig(GPIO_TypeDef* GPIOx, uint16_t GPIO_PinSource, uint8_t GPIO_AF);
# 37 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_syscfg.h" 1
# 357 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_syscfg.h"
void SYSCFG_DeInit(void);
void SYSCFG_RIDeInit(void);


void SYSCFG_MemoryRemapConfig(uint8_t SYSCFG_MemoryRemap);
void SYSCFG_USBPuCmd(FunctionalState NewState);
void SYSCFG_EXTILineConfig(uint8_t EXTI_PortSourceGPIOx, uint8_t EXTI_PinSourcex);


void SYSCFG_RITIMSelect(uint32_t TIM_Select);
void SYSCFG_RITIMInputCaptureConfig(uint32_t RI_InputCapture, uint32_t RI_InputCaptureRouting);
void SYSCFG_RIResistorConfig(uint32_t RI_Resistor, FunctionalState NewState);
void SYSCFG_RISwitchControlModeCmd(FunctionalState NewState);
void SYSCFG_RIIOSwitchConfig(uint32_t RI_IOSwitch, FunctionalState NewState);
void SYSCFG_RIHysteresisConfig(uint8_t RI_Port, uint16_t RI_Pin,
                               FunctionalState NewState);
# 38 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2



# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_pwr.h" 1
# 164 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_pwr.h"
void PWR_DeInit(void);


void PWR_RTCAccessCmd(FunctionalState NewState);


void PWR_PVDLevelConfig(uint32_t PWR_PVDLevel);
void PWR_PVDCmd(FunctionalState NewState);


void PWR_WakeUpPinCmd(uint32_t PWR_WakeUpPin, FunctionalState NewState);


void PWR_FastWakeUpCmd(FunctionalState NewState);
void PWR_UltraLowPowerCmd(FunctionalState NewState);


void PWR_VoltageScalingConfig(uint32_t PWR_VoltageScaling);


void PWR_EnterLowPowerRunMode(FunctionalState NewState);
void PWR_EnterSleepMode(uint32_t PWR_Regulator, uint8_t PWR_SLEEPEntry);
void PWR_EnterSTOPMode(uint32_t PWR_Regulator, uint8_t PWR_STOPEntry);
void PWR_EnterSTANDBYMode(void);


FlagStatus PWR_GetFlagStatus(uint32_t PWR_FLAG);
void PWR_ClearFlag(uint32_t PWR_FLAG);
# 42 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_rcc.h" 1
# 44 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_rcc.h"
typedef struct
{
  uint32_t SYSCLK_Frequency;
  uint32_t HCLK_Frequency;
  uint32_t PCLK1_Frequency;
  uint32_t PCLK2_Frequency;
}RCC_ClocksTypeDef;
# 405 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_rcc.h"
void RCC_DeInit(void);


void RCC_HSEConfig(uint8_t RCC_HSE);
ErrorStatus RCC_WaitForHSEStartUp(void);
void RCC_MSIRangeConfig(uint32_t RCC_MSIRange);
void RCC_AdjustMSICalibrationValue(uint8_t MSICalibrationValue);
void RCC_MSICmd(FunctionalState NewState);
void RCC_AdjustHSICalibrationValue(uint8_t HSICalibrationValue);
void RCC_HSICmd(FunctionalState NewState);
void RCC_LSEConfig(uint8_t RCC_LSE);
void RCC_LSICmd(FunctionalState NewState);
void RCC_PLLConfig(uint8_t RCC_PLLSource, uint8_t RCC_PLLMul, uint8_t RCC_PLLDiv);
void RCC_PLLCmd(FunctionalState NewState);
void RCC_ClockSecuritySystemCmd(FunctionalState NewState);
void RCC_MCOConfig(uint8_t RCC_MCOSource, uint8_t RCC_MCODiv);


void RCC_SYSCLKConfig(uint32_t RCC_SYSCLKSource);
uint8_t RCC_GetSYSCLKSource(void);
void RCC_HCLKConfig(uint32_t RCC_SYSCLK);
void RCC_PCLK1Config(uint32_t RCC_HCLK);
void RCC_PCLK2Config(uint32_t RCC_HCLK);
void RCC_GetClocksFreq(RCC_ClocksTypeDef* RCC_Clocks);


void RCC_RTCCLKConfig(uint32_t RCC_RTCCLKSource);
void RCC_RTCCLKCmd(FunctionalState NewState);
void RCC_RTCResetCmd(FunctionalState NewState);

void RCC_AHBPeriphClockCmd(uint32_t RCC_AHBPeriph, FunctionalState NewState);
void RCC_APB2PeriphClockCmd(uint32_t RCC_APB2Periph, FunctionalState NewState);
void RCC_APB1PeriphClockCmd(uint32_t RCC_APB1Periph, FunctionalState NewState);

void RCC_AHBPeriphResetCmd(uint32_t RCC_AHBPeriph, FunctionalState NewState);
void RCC_APB2PeriphResetCmd(uint32_t RCC_APB2Periph, FunctionalState NewState);
void RCC_APB1PeriphResetCmd(uint32_t RCC_APB1Periph, FunctionalState NewState);

void RCC_AHBPeriphClockLPModeCmd(uint32_t RCC_AHBPeriph, FunctionalState NewState);
void RCC_APB2PeriphClockLPModeCmd(uint32_t RCC_APB2Periph, FunctionalState NewState);
void RCC_APB1PeriphClockLPModeCmd(uint32_t RCC_APB1Periph, FunctionalState NewState);


void RCC_ITConfig(uint8_t RCC_IT, FunctionalState NewState);
FlagStatus RCC_GetFlagStatus(uint8_t RCC_FLAG);
void RCC_ClearFlag(void);
ITStatus RCC_GetITStatus(uint8_t RCC_IT);
void RCC_ClearITPendingBit(uint8_t RCC_IT);
# 43 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_rtc.h" 1
# 47 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_rtc.h"
typedef struct
{
  uint32_t RTC_HourFormat;


  uint32_t RTC_AsynchPrediv;


  uint32_t RTC_SynchPrediv;

}RTC_InitTypeDef;




typedef struct
{
  uint8_t RTC_Hours;




  uint8_t RTC_Minutes;


  uint8_t RTC_Seconds;


  uint8_t RTC_H12;

}RTC_TimeTypeDef;




typedef struct
{
  uint32_t RTC_WeekDay;


  uint32_t RTC_Month;


  uint8_t RTC_Date;


  uint8_t RTC_Year;

}RTC_DateTypeDef;




typedef struct
{
  RTC_TimeTypeDef RTC_AlarmTime;

  uint32_t RTC_AlarmMask;


  uint32_t RTC_AlarmDateWeekDaySel;


  uint8_t RTC_AlarmDateWeekDay;




}RTC_AlarmTypeDef;
# 528 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_rtc.h"
ErrorStatus RTC_DeInit(void);



ErrorStatus RTC_Init(RTC_InitTypeDef* RTC_InitStruct);
void RTC_StructInit(RTC_InitTypeDef* RTC_InitStruct);
void RTC_WriteProtectionCmd(FunctionalState NewState);
ErrorStatus RTC_EnterInitMode(void);
void RTC_ExitInitMode(void);
ErrorStatus RTC_WaitForSynchro(void);
ErrorStatus RTC_RefClockCmd(FunctionalState NewState);


ErrorStatus RTC_SetTime(uint32_t RTC_Format, RTC_TimeTypeDef* RTC_TimeStruct);
void RTC_TimeStructInit(RTC_TimeTypeDef* RTC_TimeStruct);
void RTC_GetTime(uint32_t RTC_Format, RTC_TimeTypeDef* RTC_TimeStruct);
ErrorStatus RTC_SetDate(uint32_t RTC_Format, RTC_DateTypeDef* RTC_DateStruct);
void RTC_DateStructInit(RTC_DateTypeDef* RTC_DateStruct);
void RTC_GetDate(uint32_t RTC_Format, RTC_DateTypeDef* RTC_DateStruct);


void RTC_SetAlarm(uint32_t RTC_Format, uint32_t RTC_Alarm, RTC_AlarmTypeDef* RTC_AlarmStruct);
void RTC_AlarmStructInit(RTC_AlarmTypeDef* RTC_AlarmStruct);
void RTC_GetAlarm(uint32_t RTC_Format, uint32_t RTC_Alarm, RTC_AlarmTypeDef* RTC_AlarmStruct);
ErrorStatus RTC_AlarmCmd(uint32_t RTC_Alarm, FunctionalState NewState);


void RTC_WakeUpClockConfig(uint32_t RTC_WakeUpClock);
void RTC_SetWakeUpCounter(uint32_t RTC_WakeUpCounter);
uint32_t RTC_GetWakeUpCounter(void);
ErrorStatus RTC_WakeUpCmd(FunctionalState NewState);


void RTC_DayLightSavingConfig(uint32_t RTC_DayLightSaving, uint32_t RTC_StoreOperation);
uint32_t RTC_GetStoreOperation(void);


void RTC_OutputConfig(uint32_t RTC_Output, uint32_t RTC_OutputPolarity);


ErrorStatus RTC_DigitalCalibConfig(uint32_t RTC_CalibSign, uint32_t Value);
ErrorStatus RTC_DigitalCalibCmd(FunctionalState NewState);
void RTC_CalibOutputCmd(FunctionalState NewState);


void RTC_TimeStampCmd(uint32_t RTC_TimeStampEdge, FunctionalState NewState);
void RTC_GetTimeStamp(uint32_t RTC_Format, RTC_TimeTypeDef* RTC_StampTimeStruct,
                                      RTC_DateTypeDef* RTC_StampDateStruct);



void RTC_TamperTriggerConfig(uint32_t RTC_Tamper, uint32_t RTC_TamperTrigger);
void RTC_TamperCmd(uint32_t RTC_Tamper, FunctionalState NewState);


void RTC_WriteBackupRegister(uint32_t RTC_BKP_DR, uint32_t Data);
uint32_t RTC_ReadBackupRegister(uint32_t RTC_BKP_DR);


void RTC_OutputTypeConfig(uint32_t RTC_OutputType);



void RTC_ITConfig(uint32_t RTC_IT, FunctionalState NewState);
FlagStatus RTC_GetFlagStatus(uint32_t RTC_FLAG);
void RTC_ClearFlag(uint32_t RTC_FLAG);
ITStatus RTC_GetITStatus(uint32_t RTC_IT);
void RTC_ClearITPendingBit(uint32_t RTC_IT);
# 44 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2
# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_spi.h" 1
# 48 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_spi.h"
typedef struct
{
  uint16_t SPI_Direction;


  uint16_t SPI_Mode;


  uint16_t SPI_DataSize;


  uint16_t SPI_CPOL;


  uint16_t SPI_CPHA;


  uint16_t SPI_NSS;



  uint16_t SPI_BaudRatePrescaler;





  uint16_t SPI_FirstBit;


  uint16_t SPI_CRCPolynomial;
}SPI_InitTypeDef;
# 334 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_spi.h"
void SPI_I2S_DeInit(SPI_TypeDef* SPIx);


void SPI_Init(SPI_TypeDef* SPIx, SPI_InitTypeDef* SPI_InitStruct);
void SPI_StructInit(SPI_InitTypeDef* SPI_InitStruct);
void SPI_Cmd(SPI_TypeDef* SPIx, FunctionalState NewState);
void SPI_DataSizeConfig(SPI_TypeDef* SPIx, uint16_t SPI_DataSize);
void SPI_BiDirectionalLineConfig(SPI_TypeDef* SPIx, uint16_t SPI_Direction);
void SPI_NSSInternalSoftwareConfig(SPI_TypeDef* SPIx, uint16_t SPI_NSSInternalSoft);
void SPI_SSOutputCmd(SPI_TypeDef* SPIx, FunctionalState NewState);


void SPI_I2S_SendData(SPI_TypeDef* SPIx, uint16_t Data);
uint16_t SPI_I2S_ReceiveData(SPI_TypeDef* SPIx);


void SPI_CalculateCRC(SPI_TypeDef* SPIx, FunctionalState NewState);
void SPI_TransmitCRC(SPI_TypeDef* SPIx);
uint16_t SPI_GetCRC(SPI_TypeDef* SPIx, uint8_t SPI_CRC);
uint16_t SPI_GetCRCPolynomial(SPI_TypeDef* SPIx);


void SPI_I2S_DMACmd(SPI_TypeDef* SPIx, uint16_t SPI_I2S_DMAReq, FunctionalState NewState);


void SPI_I2S_ITConfig(SPI_TypeDef* SPIx, uint8_t SPI_I2S_IT, FunctionalState NewState);
FlagStatus SPI_I2S_GetFlagStatus(SPI_TypeDef* SPIx, uint16_t SPI_I2S_FLAG);
void SPI_I2S_ClearFlag(SPI_TypeDef* SPIx, uint16_t SPI_I2S_FLAG);
ITStatus SPI_I2S_GetITStatus(SPI_TypeDef* SPIx, uint8_t SPI_I2S_IT);
void SPI_I2S_ClearITPendingBit(SPI_TypeDef* SPIx, uint8_t SPI_I2S_IT);
# 45 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2

# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_usart.h" 1
# 48 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_usart.h"
typedef struct
{
  uint32_t USART_BaudRate;




  uint16_t USART_WordLength;


  uint16_t USART_StopBits;


  uint16_t USART_Parity;






  uint16_t USART_Mode;


  uint16_t USART_HardwareFlowControl;


} USART_InitTypeDef;





typedef struct
{

  uint16_t USART_Clock;


  uint16_t USART_CPOL;


  uint16_t USART_CPHA;


  uint16_t USART_LastBit;


} USART_ClockInitTypeDef;
# 341 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_usart.h"
void USART_DeInit(USART_TypeDef* USARTx);


void USART_Init(USART_TypeDef* USARTx, USART_InitTypeDef* USART_InitStruct);
void USART_StructInit(USART_InitTypeDef* USART_InitStruct);
void USART_ClockInit(USART_TypeDef* USARTx, USART_ClockInitTypeDef* USART_ClockInitStruct);
void USART_ClockStructInit(USART_ClockInitTypeDef* USART_ClockInitStruct);
void USART_Cmd(USART_TypeDef* USARTx, FunctionalState NewState);
void USART_SetPrescaler(USART_TypeDef* USARTx, uint8_t USART_Prescaler);
void USART_OverSampling8Cmd(USART_TypeDef* USARTx, FunctionalState NewState);
void USART_OneBitMethodCmd(USART_TypeDef* USARTx, FunctionalState NewState);


void USART_SendData(USART_TypeDef* USARTx, uint16_t Data);
uint16_t USART_ReceiveData(USART_TypeDef* USARTx);


void USART_SetAddress(USART_TypeDef* USARTx, uint8_t USART_Address);
void USART_WakeUpConfig(USART_TypeDef* USARTx, uint16_t USART_WakeUp);
void USART_ReceiverWakeUpCmd(USART_TypeDef* USARTx, FunctionalState NewState);


void USART_LINBreakDetectLengthConfig(USART_TypeDef* USARTx, uint16_t USART_LINBreakDetectLength);
void USART_LINCmd(USART_TypeDef* USARTx, FunctionalState NewState);
void USART_SendBreak(USART_TypeDef* USARTx);


void USART_HalfDuplexCmd(USART_TypeDef* USARTx, FunctionalState NewState);


void USART_SmartCardCmd(USART_TypeDef* USARTx, FunctionalState NewState);
void USART_SmartCardNACKCmd(USART_TypeDef* USARTx, FunctionalState NewState);
void USART_SetGuardTime(USART_TypeDef* USARTx, uint8_t USART_GuardTime);


void USART_IrDAConfig(USART_TypeDef* USARTx, uint16_t USART_IrDAMode);
void USART_IrDACmd(USART_TypeDef* USARTx, FunctionalState NewState);


void USART_DMACmd(USART_TypeDef* USARTx, uint16_t USART_DMAReq, FunctionalState NewState);


void USART_ITConfig(USART_TypeDef* USARTx, uint16_t USART_IT, FunctionalState NewState);
FlagStatus USART_GetFlagStatus(USART_TypeDef* USARTx, uint16_t USART_FLAG);
void USART_ClearFlag(USART_TypeDef* USARTx, uint16_t USART_FLAG);
ITStatus USART_GetITStatus(USART_TypeDef* USARTx, uint16_t USART_IT);
void USART_ClearITPendingBit(USART_TypeDef* USARTx, uint16_t USART_IT);
# 47 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2

# 1 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/misc.h" 1
# 48 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/misc.h"
typedef struct
{
  uint8_t NVIC_IRQChannel;




  uint8_t NVIC_IRQChannelPreemptionPriority;



  uint8_t NVIC_IRQChannelSubPriority;



  FunctionalState NVIC_IRQChannelCmd;


} NVIC_InitTypeDef;
# 176 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/misc.h"
void NVIC_PriorityGroupConfig(uint32_t NVIC_PriorityGroup);
void NVIC_Init(NVIC_InitTypeDef* NVIC_InitStruct);
void NVIC_SetVectorTable(uint32_t NVIC_VectTab, uint32_t Offset);
void NVIC_SystemLPConfig(uint8_t LowPowerMode, FunctionalState NewState);
void SysTick_CLKSourceConfig(uint32_t SysTick_CLKSource);
# 49 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\src/stm32l1xx_conf.h" 2
# 5100 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\CMSIS\\CM3\\DeviceSupport\\ST\\STM32L1xx/stm32l1xx.h" 2
# 33 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_wwdg.h" 2
# 75 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\inc/stm32l1xx_wwdg.h"
void WWDG_DeInit(void);


void WWDG_SetPrescaler(uint32_t WWDG_Prescaler);
void WWDG_SetWindowValue(uint8_t WindowValue);
void WWDG_EnableIT(void);
void WWDG_SetCounter(uint8_t Counter);


void WWDG_Enable(uint8_t Counter);


FlagStatus WWDG_GetFlagStatus(void);
void WWDG_ClearFlag(void);
# 81 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c" 2
# 136 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c"
void WWDG_DeInit(void)
{
  RCC_APB1PeriphResetCmd(((uint32_t)0x00000800), ENABLE);
  RCC_APB1PeriphResetCmd(((uint32_t)0x00000800), DISABLE);
}
# 152 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c"
void WWDG_SetPrescaler(uint32_t WWDG_Prescaler)
{
  uint32_t tmpreg = 0;

  ((void)0);

  tmpreg = ((WWDG_TypeDef *) (((uint32_t)0x40000000) + 0x2C00))->CFR & ((uint32_t)0xFFFFFE7F);

  tmpreg |= WWDG_Prescaler;

  ((WWDG_TypeDef *) (((uint32_t)0x40000000) + 0x2C00))->CFR = tmpreg;
}







void WWDG_SetWindowValue(uint8_t WindowValue)
{
  volatile uint32_t tmpreg = 0;


  ((void)0);


  tmpreg = ((WWDG_TypeDef *) (((uint32_t)0x40000000) + 0x2C00))->CFR & ((uint32_t)0xFFFFFF80);


  tmpreg |= WindowValue & (uint32_t) ((uint8_t)0x7F);


  ((WWDG_TypeDef *) (((uint32_t)0x40000000) + 0x2C00))->CFR = tmpreg;
}







void WWDG_EnableIT(void)
{
  *(volatile uint32_t *) (((uint32_t)0x42000000) + ((((((uint32_t)0x40000000) + 0x2C00) - ((uint32_t)0x40000000)) + 0x04) * 32) + (0x09 * 4)) = (uint32_t)ENABLE;
}
# 206 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c"
void WWDG_SetCounter(uint8_t Counter)
{

  ((void)0);


  ((WWDG_TypeDef *) (((uint32_t)0x40000000) + 0x2C00))->CR = Counter & ((uint8_t)0x7F);
}
# 238 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c"
void WWDG_Enable(uint8_t Counter)
{

  ((void)0);
  ((WWDG_TypeDef *) (((uint32_t)0x40000000) + 0x2C00))->CR = ((uint8_t)0x80) | Counter;
}
# 266 "C:\\Users\\Mytka\\Documents\\Atollic True Studio Workspace\\podst_prog\\Libraries\\STM32L1xx_StdPeriph_Driver\\src\\stm32l1xx_wwdg.c"
FlagStatus WWDG_GetFlagStatus(void)
{
  FlagStatus bitstatus = RESET;

  if ((((WWDG_TypeDef *) (((uint32_t)0x40000000) + 0x2C00))->SR) != (uint32_t)RESET)
  {
    bitstatus = SET;
  }
  else
  {
    bitstatus = RESET;
  }
  return bitstatus;
}






void WWDG_ClearFlag(void)
{
  ((WWDG_TypeDef *) (((uint32_t)0x40000000) + 0x2C00))->SR = (uint32_t)RESET;
}
