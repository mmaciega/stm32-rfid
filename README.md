# STM32 RFID #

Celem projektu była zaprojektowanie obsługi czytnika kart zbliżeniowych RFID na bazie
mikroprocesora STM32. Urządzenie miało zliczać czas aktywności danego breloczka oraz
wyświetlać wyniki na wyświetlaczu LCD z Nokii 3410.

Do komunikacji między modułem RFID a mikroprocesorem zastosowaliśmy układ USART służący
do konwersji danych przychodzących do mikroprocesora poprzez port szeregowy natomiast do
komunikacji między mikroprocesorem a wyświetlaczem wykorzystaliśmy szeregowy interfejs
urządzeń peryferyjnych SPI.

W projekcie zostały użyete dwa przerwania:

* Przerwanie timera SysTick (przerwanie co 1ms), które odpowiedzialne jest za zliczanie
czasu aktywności breloczków,
* Przerwanie USART (inicjowane tylko dla buforu odbiorczego), które odpowiedzialne jest za
obsługę otrzymywanych ramek z danymi (z modułu RFID do mikroprocesora).

Aplikacja została napisana w środowisu programistycznym Atollic True Studio.

### Wykorzystane elementy ###
* STM32L-Discovery
* MM-RFID1
* Interfejs USB-RS232 (TTL) EM-216 EM-USB-DIL24
* Wyświetlacz LCD Nokia 3410
* Transponder RFID Brelok 125kHz Unique FV
* Przewody połączeniowe